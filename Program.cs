﻿using Microsoft.Win32;
using System;

namespace DotNetFrameworkVersion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Get45PlusFromRegistry();
        }

        private static void Get45PlusFromRegistry()
        {
            using (RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
            {
                if (registryKey != null && registryKey.GetValue("Release") != null)
                {
                    string text = CheckFor45PlusVersion((int)registryKey.GetValue("Release"));
                    if (string.IsNullOrEmpty(text))
                    {
                        GetVersionFromRegistry();
                        Console.ReadKey();
                        return;
                    }
                    Console.WriteLine("");
                    Console.WriteLine("  ==========================================");
                    Console.WriteLine("  最新 .NET Framework 版本: " + text);
                    Console.WriteLine("  ==========================================");
                    Console.WriteLine("按任意键退出...");
                    Console.ReadKey(true);
                    return;
                }
                Console.WriteLine("");
                Console.WriteLine("  ==========================================");
                Console.WriteLine("  未安装任何版本 .NET Framework");
                Console.WriteLine("  ==========================================");
                Console.WriteLine("  按任意键退出...");
                Console.ReadKey(true);
            }
        }

        static string CheckFor45PlusVersion(int releaseKey)
        {
            if (releaseKey >= 528040)
            {
                return "4.8 或更高版本";
            }
            if (releaseKey >= 461808)
            {
                return "4.7.2";
            }
            if (releaseKey >= 461308)
            {
                return "4.7.1";
            }
            if (releaseKey >= 460798)
            {
                return "4.7";
            }
            if (releaseKey >= 394802)
            {
                return "4.6.2";
            }
            if (releaseKey >= 394254)
            {
                return "4.6.1";
            }
            if (releaseKey >= 393295)
            {
                return "4.6";
            }
            if (releaseKey >= 379893)
            {
                return "4.5.2";
            }
            if (releaseKey >= 378675)
            {
                return "4.5.1";
            }
            if (releaseKey >= 378389)
            {
                return "4.5";
            }
            return "未安装.NET Framework 或更高版本";
        }

        private static void GetVersionFromRegistry()
        {
            RegistryKey registryKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\");
            string[] subKeyNames = registryKey.GetSubKeyNames();
            foreach (string text in subKeyNames)
            {
                if (text == "v4" || !text.StartsWith("v"))
                {
                    continue;
                }
                RegistryKey registryKey2 = registryKey.OpenSubKey(text);
                string text2 = (string)registryKey2.GetValue("Version", "");
                string text3 = registryKey2.GetValue("SP", "").ToString();
                string text4 = registryKey2.GetValue("Install", "").ToString();
                if (string.IsNullOrEmpty(text4))
                {
                    Console.WriteLine(text + "  " + text2);
                }
                else if (!string.IsNullOrEmpty(text3) && text4 == "1")
                {
                    Console.WriteLine(text + "  " + text2 + "  SP" + text3);
                }
                if (!string.IsNullOrEmpty(text2))
                {
                    continue;
                }
                string[] subKeyNames2 = registryKey2.GetSubKeyNames();
                foreach (string text5 in subKeyNames2)
                {
                    RegistryKey registryKey3 = registryKey2.OpenSubKey(text5);
                    text2 = (string)registryKey3.GetValue("Version", "");
                    if (!string.IsNullOrEmpty(text2))
                    {
                        text3 = registryKey3.GetValue("SP", "").ToString();
                    }
                    text4 = registryKey3.GetValue("Install", "").ToString();
                    if (string.IsNullOrEmpty(text4))
                    {
                        Console.WriteLine(text + "  " + text2);
                    }
                    else if (!string.IsNullOrEmpty(text3) && text4 == "1")
                    {
                        Console.WriteLine(text5 + "  " + text2 + "  SP" + text3);
                    }
                    else if (text4 == "1")
                    {
                        Console.WriteLine("  " + text5 + "  " + text2);
                    }
                }
            }
        }
    }
}
